# -*- coding: utf-8 -*-

# null.py
# copyright 2014 яша <yasha@xyzzy.be>

# This file is part of xx.
#
# xx is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# xx is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

import os
import node
import subprocess

class null(node.node):
    def __init__(self):
        BaseClass.__init__(self)
        self.sink = os.devnull
    def start(self):
        if len(self.sources) is not 1:
            raise RuntimeError("exactly 1 source required")
        if self.off:
            with open(os.devnull, "w") as devnull:
                self.proc = subprocess.Popen(["/usr/bin/cat",
                                              self.sources[0].sink],
                                             stdin=devnull,
                                             stdout=devnull,
                                             stderr=devnull)
